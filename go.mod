module gitlab.com/rawkode/gitseries-go

go 1.14

require (
	github.com/libgit2/git2go/v30 v30.0.3
	github.com/sourcegraph/go-diff v0.5.3
	github.com/spf13/cobra v1.0.0
)
