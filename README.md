# Gitseries (Go version)

This is the Go rewrite of [Gitseries](https://gitlab.com/rawkode/gitseries), which was written in Rust. Unfortunately, at the current time, libgit2 for Rust does not support async/await and concurrency; which renders the Rust version difficult and slow to use for larger repositories.

## What?

Gitseries is a project that aims to collect metrics from your Git repositories for analyis within a time series database. Gitseries outputs those metrics in InfluxDB's line protocol format. However, using Telegraf, you can write this output to ANY other time series database that supports event / irregular time series.

## How?

You'll need to compile this project from source. That's really easy to do 😁

```console
go build ./...
```

Now you can do the interesting things. It's usually best to write this output to a file.

```console
./gitseries analysis -d <directory to git repo> > line_protocol.txt
```

This can then be loaded into your time-series database. I've provided a [telegraf.conf](./telegraf.conf) file that shows how to do this for InfluxDB.

```console
telegraf --config ./telegraf.conf
```
