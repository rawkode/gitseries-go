package cmds

import (
	"fmt"
	"os"

	_ "github.com/libgit2/git2go/v30"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "gitseries",
	Short: "Analyse your Git repository",
	Long:  `Sure thing boss`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
