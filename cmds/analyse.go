package cmds

import (
	"fmt"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	git2 "github.com/libgit2/git2go/v30"
	_ "github.com/sourcegraph/go-diff/diff"
	"github.com/spf13/cobra"
	"gitlab.com/rawkode/gitseries-go/git"
	"gitlab.com/rawkode/gitseries-go/influxdb"
)

var analyseCmd = &cobra.Command{
	Use:   "analyse",
	Short: "Analyse",
	Long:  `Analyse`,
	Run:   analyse,
}

func init() {
	analyseCmd.Flags().StringP("directory", "d", ".", "Path to the Git clone directory")
	rootCmd.AddCommand(analyseCmd)
}

func analyse(cmd *cobra.Command, args []string) {
	var wg sync.WaitGroup

	directory, _ := cmd.Flags().GetString("directory")

	repo, err := git.ValidateDirectory(directory)
	if err != nil {
		fmt.Printf("Invalid Repository: %v\n", err)
		return
	}

	// Fetch "metadata" for Repo
	metadata, err := git.GetMetadata(repo)
	if err != nil {
		fmt.Printf("Failed to fetch metadata from repo. This may be because there was no remote named 'origin'. %v", err)
		return
	}

	// Tags are sometimes Commits
	repo.Tags.Foreach(func(name string, oid *git2.Oid) error {
		object, err := repo.Lookup(oid)

		if err != nil {
			return err
		}

		maybeTag, err := object.Peel(git2.ObjectTag)
		if err == nil {
			tagTag, err := maybeTag.AsTag()
			if err != nil {
				return err
			}

			wg.Add(1)
			go processTag(tagTag, metadata, &wg)
			return nil
		}

		maybeCommit, err := object.Peel(git2.ObjectCommit)
		if err == nil {
			commitTag, err := maybeCommit.AsCommit()
			if err != nil {
				return err
			}

			wg.Add(1)
			go processTagCommit(commitTag, metadata, &wg)
			return nil
		}

		return nil

	})

	repo.Head()
	walk, _ := repo.Walk()

	// Walk backwards through every commit
	walk.PushHead()
	walk.Iterate(func(commit *git2.Commit) bool {
		wg.Add(1)
		go processCommit(repo, commit, metadata, &wg)
		return true
	})

	wg.Wait()
}

func processCommit(repo *git2.Repository, commit *git2.Commit, metadata *git.Metadata, wg *sync.WaitGroup) {
	defer wg.Done()

	metatags := fmt.Sprintf("host=%v,owner=%v,repository=%v", metadata.Host, metadata.Owner, metadata.Name)

	message := influxdb.StringFieldEscape(commit.Summary())
	authorEmail := influxdb.Escape(commit.Committer().Email)
	authorName := influxdb.Escape(commit.Author().Name)
	sha := influxdb.StringFieldEscape(commit.Id().String())

	tags := fmt.Sprintf("author_email=%v,author_name=%v", authorEmail, authorName)

	// Is the commit semantic?
	r := regexp.MustCompile(`^(?P<type>\w+)[:\(](?:(?P<component>\w+))?`)
	matches := r.FindStringSubmatch(commit.Summary())

	if len(matches) == 3 {
		if matches[1] != "" {
			tags = fmt.Sprintf("%v,type=%v", tags, matches[1])
		}
		if matches[2] != "" {
			tags = fmt.Sprintf("%v,component=%v", tags, matches[2])
		}
	}

	if commit.ParentCount() < 1 || commit.ParentCount() > 1 {
		fmt.Printf("commit,%v,%v,stats_failed=1 sha=\"%v\",message=\"%v\" %v\n", metatags, tags, sha, message, commit.Author().When.UnixNano())
		return
	}

	parentCommit := commit.Parent(0)

	parentObject, _ := parentCommit.Peel(git2.ObjectTree)
	currentObject, _ := commit.Peel(git2.ObjectTree)

	parentTree, _ := parentObject.AsTree()
	currentTree, _ := currentObject.AsTree()

	diff, _ := repo.DiffTreeToTree(parentTree, currentTree, nil)

	stats, err := diff.Stats()
	if err != nil {
		fmt.Printf("commit,%v,%v,stats_failed=1 sha=\"%v\",message=\"%v\" %v\n", metatags, tags, sha, message, commit.Author().When.UnixNano())
		return
	}

	insertions := stats.Insertions()
	deletions := stats.Deletions()
	filesModified := stats.FilesChanged()

	err = diff.ForEach(func(file git2.DiffDelta, progress float64) (git2.DiffForEachHunkCallback, error) {
		var extension = influxdb.Escape(filepath.Ext(file.OldFile.Path))
		var filename = influxdb.Escape(filepath.Base(file.OldFile.Path))
		var directory = influxdb.Escape(filepath.Dir(file.OldFile.Path))

		if extension == "" {
			extension = "none"
		}

		switch file.Status {
		case git2.DeltaAdded:
			fmt.Printf("file,%v,status=new,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break
		case git2.DeltaDeleted:
			fmt.Printf("file,%v,status=deleted,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break
		case git2.DeltaModified:
			fmt.Printf("file,%v,status=modified,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break
		case git2.DeltaCopied:
			fmt.Printf("file,%v,status=copied,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break
		case git2.DeltaRenamed:
			fmt.Printf("file,%v,status=renamed,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break
		case git2.DeltaTypeChange:
			fmt.Printf("file,%v,status=type_changed,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v similarity=%v %v\n", metatags, directory, filename, extension, authorName, authorEmail, file.Similarity, commit.Author().When.UnixNano())
			break

		}

		// Maybe hunks and line changes is taking it too far ...
		return func(hunk git2.DiffHunk) (git2.DiffForEachLineCallback, error) {
			return func(line git2.DiffLine) error {
				var contents = influxdb.StringFieldEscape(line.Content)
				contents = strings.ReplaceAll(contents, "\n", "\\\\n")
				contents = strings.ReplaceAll(contents, "\t", "\\\\t")

				comment := false
				if strings.HasPrefix(line.Content, "//") || strings.HasPrefix(line.Content, "/*") || strings.HasPrefix(line.Content, "#") {
					comment = true
				}

				switch line.Origin {
				case git2.DiffLineAddition:
					fmt.Printf("line,%v,status=added,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v,comment=%v contents=\"%v\" %v\n", metatags, directory, filename, extension, authorName, authorEmail, comment, contents, commit.Author().When.UnixNano())
					break
				case git2.DiffLineDeletion:
					fmt.Printf("line,%v,status=deleted,directory=%v,filename=%v,extension=%v,author_name=%v,author_email=%v,comment=%v contents=\"%v\" %v\n", metatags, directory, filename, extension, authorName, authorEmail, comment, contents, commit.Author().When.UnixNano())
					break
				}
				return nil
			}, nil
		}, nil
	}, git2.DiffDetailLines)

	fmt.Printf("commit,%v,%v files_modified=%d,insertions=%d,deletions=%d,sha=\"%v\",message=\"%v\" %v\n", metatags, tags, filesModified, insertions, deletions, sha, message, commit.Author().When.UnixNano())

	return
}

func processTag(tag *git2.Tag, metadata *git.Metadata, wg *sync.WaitGroup) {
	name := influxdb.StringFieldEscape(tag.Name())
	authorEmail := influxdb.Escape(tag.Tagger().Email)
	authorName := influxdb.Escape(tag.Tagger().Name)

	fmt.Printf("tag,author_email=%v,author_name=%v,host=%v,owner=%v,repository=%v name=\"%v\" %v\n", authorEmail, authorName, metadata.Host, metadata.Owner, metadata.Name, name, tag.Tagger().When.UnixNano())

	wg.Done()
}

func processTagCommit(tag *git2.Commit, metadata *git.Metadata, wg *sync.WaitGroup) {
	name := influxdb.StringFieldEscape(tag.Summary())
	authorEmail := influxdb.Escape(tag.Author().Email)
	authorName := influxdb.Escape(tag.Author().Name)

	fmt.Printf("tag,author_email=%v,author_name=%v,host=%v,owner=%v,repository=%v name=\"%v\" %v\n", authorEmail, authorName, metadata.Host, metadata.Owner, metadata.Name, name, tag.Author().When.UnixNano())

	wg.Done()
}
