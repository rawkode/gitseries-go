package git

import (
	git2 "github.com/libgit2/git2go/v30"
)

func ValidateDirectory(directory string) (*git2.Repository, error) {
	repo, err := git2.OpenRepository(directory)
	if err != nil {
		return nil, err
	}

	return repo, nil
}
