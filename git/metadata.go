package git

import (
	"regexp"

	git2 "github.com/libgit2/git2go/v30"
)

type Metadata struct {
	Host  string
	Owner string
	Name  string
}

func GetMetadata(repo *git2.Repository) (*Metadata, error) {
	remote, err := repo.Remotes.Lookup("origin")
	if err != nil {
		return nil, err
	}

	r := regexp.MustCompile(`^(?:\S+:\/\/)?(?:\S+@)?(?P<host>\S+?)[:\/](?P<owner>\S+?)[\/](?P<repository>\S+)$`)
	matches := r.FindStringSubmatch(remote.Url())

	// url, err := url.Parse(remote.Url())
	// if err != nil {
	// 	return nil, err
	// }

	metadata := Metadata{
		Host:  matches[1],
		Owner: matches[2],
		Name:  matches[3],
	}

	return &metadata, nil
}
