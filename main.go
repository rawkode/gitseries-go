package main

import (
	_ "github.com/libgit2/git2go/v30"
	"gitlab.com/rawkode/gitseries-go/cmds"
)

func main() {
	cmds.Execute()
}
