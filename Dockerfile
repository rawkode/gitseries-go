FROM archlinux

RUN pacman -Sy --noconfirm git go libgit2

COPY . /code
WORKDIR /code

RUN go build -o gitseries main.go